## Ensembl Ticket Manager

- An application to manage support tickets created through contact_us form on 2020.ensembl.org.

### Quick Start
- `$ git clone git clone git@gitlab.ebi.ac.uk:ensembl-web/ensembl_ticket_manager.git`
- `$ cd ensembl_ticket_manager`
- `$ sudo docker-compose -f docker-compose.yml up`

To run as a daemon run:
- `$ sudo docker-compose -f docker-compose.yml up -d`

A request sample working with current configurations:
- `http://0.0.0.0:8083/api/support/ticket` 


How to run unit tests
- `$ sudo docker exec -it [DOCKER_CONTAINER_NAME] python -m unittest`


## Development
Use [Python Black](https://pypi.org/project/black/) package as code formatter.

