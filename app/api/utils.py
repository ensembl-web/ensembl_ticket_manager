"""
See the NOTICE file distributed with this work for additional information
regarding copyright ownership.


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import asyncio
import logging
from loguru import logger
import jwt
import os
from jwt import ExpiredSignatureError
from core.ensembl_logging import InterceptHandler
from core.config import JWT_ALGORITHM, JWT_SECRET

logging.getLogger().handlers = [InterceptHandler()]

async def validate_jwt_token(jwt_token: str):
    payload = jwt.decode(jwt_token, JWT_SECRET, algorithms=[JWT_ALGORITHM])
    return True