"""
See the NOTICE file distributed with this work for additional information
regarding copyright ownership.


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import logging

from aiohttp import ClientResponseError
import datetime
from fastapi import APIRouter, Request, Form, File, UploadFile, Response
from fastapi.responses import HTMLResponse
import json
from jwt import ExpiredSignatureError
from loguru import logger
from pydantic import EmailStr
from typing import Optional, List

from api.error_response import response_error_handler
from api.resources.models import TicketResponse
from api.resources.mail_manager import mail_manager
from api.utils import validate_jwt_token
from core.ensembl_logging import InterceptHandler

logging.getLogger().handlers = [InterceptHandler()]

router = APIRouter()

@router.get("/index", name="index")
async def index_page(request:Request):
    """

    """
    html_content='''
    <!DOCTYPE html>
    <html>
    <body>

    <h1>Ensembl Contact us</h1>

    <form method="post" action="/api/support/ticket" enctype="multipart/form-data">
        <label size="25">name </label>
        <input name="name" id="name" type="text" size="50"><br>
        <label>email </label>
        <input name="email" id="email" type="text" size="50"><br>
        <label>subject </label>
        <input name="subject" id="subject" type="text" size="50" value="IGNORE : Testing Attachments"><br>
        <label>message </label>
        <textarea name="message" id="message" type="textarea" rows="10" cols="50">
Helpdesk, Please ignore. Testing ticket creation for 2020.ensembl.org. It will be cleaned up by EOD</textarea><br>
        <label for="files">Select files:</label>
        <input type="file" id="files" name="files" multiple><br>
        <input type="submit">
    </form>
    </body>
    </html>
    '''

    return HTMLResponse(content=html_content, status_code=200)

@router.post("/ticket", name="attchments", response_model=TicketResponse, status_code=201)
async def create_ticket_attachment(request: Request,
                                   name: str = Form(...),
                                   email: EmailStr = Form(...),
                                   subject: str = Form(...),
                                   message: str = Form(...),
                                   files: Optional[List[UploadFile]] = File(None)):
    """
    Create ticket by sending an email to helpdesk
    """
    response = Response()
    is_token_valid = False
    error_message = ""
    try:
        token = request.headers['Authorization']
        is_token_valid = await validate_jwt_token(token.replace('Bearer ','').strip())
        if is_token_valid:
            ticket_data = {
                "name": name,
                "email": email,
                "subject": subject,
                "message": message,
                "files": files,
                "request": request,
                "date" : datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
            }
            email_success = await mail_manager.send_email_wtih_attachment_async(ticket_data)
            if email_success:
                response = TicketResponse()
                response.success = True
            else:
                response.status_code = 500
                error_message = "Could not send email"
                logger.debug(f"Could not send email and RT is not created")
        else:
            response.status_code = 401
            error_message = "Token Invalid"
    except KeyError as ke:
        response.status_code = 401
        error_message = ke
        logger.debug(f"{ke}")
    except ExpiredSignatureError as ese:
        response.status_code = 401
        error_message = ese
        logger.debug(f"{ese}")
    except Exception as ex:
        response.status_code = 500
        error_message = ex
        logger.debug(f"{ex}")
    return response
