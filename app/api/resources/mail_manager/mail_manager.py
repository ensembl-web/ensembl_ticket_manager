import os
from dotenv import load_dotenv
import asyncio
from email.header import Header
from email.utils import formataddr
from email.message import EmailMessage
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import aiosmtplib
from aiosmtplib.errors import SMTPException
from loguru import logger
from jinja2 import Environment, FileSystemLoader
from jinja2 import Template

load_dotenv()
CURR_DIR = os.path.dirname(os.path.abspath(__file__))
env = Environment(loader=FileSystemLoader(os.path.join(CURR_DIR,"templates")))
email_template = env.get_template("contact_us.j2")

class Envs:
    MAIL_SERVER = os.getenv('MAIL_SERVER')
    MAIL_PORT = os.getenv('MAIL_PORT')
    HELPDESK_EMAIL = os.getenv('HELPDESK_EMAIL')

async def format_message(tq):
    formated_message = email_template.render(tq)
    return formated_message

@logger.catch
async def send_email_wtih_attachment_async(tq):
    try:
        message = EmailMessage()
        message["From"] = formataddr((str(Header(tq["name"], 'utf-8')),tq["email"]))
        message["To"] = Envs.HELPDESK_EMAIL
        message["Subject"] = f"{tq['subject']} - {tq['request'].headers['Host']}"
        body = await format_message(tq)
        message.set_content(body)
        if tq["files"] is not None:
            for attachment in tq["files"]:
                maintype, subtype = attachment.content_type.split('/', 1)
                message.add_attachment(attachment.file.read(),
                                      maintype=maintype, 
                                      subtype=subtype,
                                      filename=attachment.filename)
        await aiosmtplib.send(message, hostname=Envs.MAIL_SERVER, port=Envs.MAIL_PORT)
        logger.info(f"The email sent successfully for {tq['email']}")
        return True
    except SMTPException as smtp_ex:
        logger.debug(smtp_ex)
    except Exception as ex:
        logger.debug(ex)
    return False
