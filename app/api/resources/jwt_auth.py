"""
See the NOTICE file distributed with this work for additional information
regarding copyright ownership.


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import logging

from aiohttp import ClientResponseError
from typing import Optional, List
from fastapi import APIRouter, Request
from api.error_response import response_error_handler
from core.ensembl_logging import InterceptHandler
from core.config import JWT_EXP_DELTA_SECONDS, JWT_ALGORITHM, JWT_SECRET
from api.resources.models import TokenResponse

from loguru import logger
import json
import jwt
import os
from datetime import datetime, timedelta

logging.getLogger().handlers = [InterceptHandler()]

router = APIRouter()

@router.get("/token", name="token", response_model=TokenResponse)
async def send_jwt_token(request:Request):
    """
    Returns json payload containing JWT Token
    """
    payload = {
        'exp': datetime.utcnow() + timedelta(seconds=JWT_EXP_DELTA_SECONDS)
    }
    jwt_token = jwt.encode(payload, JWT_SECRET, JWT_ALGORITHM)
    return {'token': jwt_token}
